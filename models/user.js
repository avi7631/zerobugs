
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name:{type:String, max:100},
    email:{type:String, required:true, max:200},
    password:{type:String, required:true, max:100},
    userType: {type:String},
    loginToken:{type:String},
    isLogIn:{type:Boolean},
    resetToken: {type: String},
    resetTokenExpiration: {type: Date}
});

module.exports = mongoose.model('User', userSchema);
