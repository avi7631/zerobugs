
const router = require('express').Router();
const home = require('../controller/home/index');

router.get('/', home.index );

module.exports = router;