
const router = require('express').Router();
const validation = require('../middleware/validation');

const adminAuth = require('../controller/admin/auth');
const adminDash = require('../controller/admin/dashboard');

router.get(['/','/login'], adminAuth.getLogin);
router.post('/login', validation.loginCheck(),  adminAuth.postLogin);

router.get('/logout', adminAuth.getLogout);

// router.get('/signup', custAuth.getSignup);
// router.post('/signup', validation.signupCheck(),  custAuth.postSignup);

router.get('/dashboard', adminDash.getDashboard);

module.exports = router;



