
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser')
const session = require('express-session');
const csrf = require('csurf');
const flash = require('connect-flash');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cookieParser());

let session_secret = 'qwertyuipolkjhgfasd!@#$%^&*(){}|_+~zxcvbnm';
app.use( session({secret: session_secret, resave: false, saveUninitialized: false}) );

let csrfProtection = csrf({
    cookie:true
});

app.use(csrfProtection);
app.use( flash() );

app.use( (req, res, next)=>{
    res.locals.csrfToken = req.csrfToken();
    next();
})

/** Routes Inisilize here */
const landingRoutes = require('./routes/home');
const adminRoutes = require('./routes/superUser');

app.use('/', landingRoutes);
app.use('/super', adminRoutes);


/**
 * Database function start here 
 * Database connect by Mongoose name: zerobugs.
 * Set option as per requirement 
 * find tuts from medium about the mongodb options 
 */
mongoose.set('useNewUrlParser', true);
// mongoose.set('useFindAndModify', false);
// mongoose.set('useCreateIndex', true);
 mongoose.set('useUnifiedTopology', true);

const db = require('./config/db');
mongoose.connect(db.MDB_URI, (errM, resM)=> {
    app.listen(3000, (errServer)=>{
         if(errServer) 
            console.log(errServer);
        else{
            console.log("Server Start: 3000");
        }
         
    });
 });
/** END Mongoose Connection */

app.use( (error, req, res, next) => {
    console.log(error);
    res.redirect('/');
});