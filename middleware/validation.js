

const { check } = require('express-validator');
const User = require('../models/user');

exports.loginCheck = ()=>{

    return [
                
        check('email', 'Please Enter valid and Password Email.').isEmail().normalizeEmail().escape(),
        check('password', 'Please Enter valid and Password Email.').notEmpty().escape()
    ];
}

exports.signupCheck = () => {

    return [
        check('name', 'Name must be at least 3 characters and valid.').notEmpty().trim().escape().isLength({min:3,max:100}),
        check('email', 'Please enter valid email.').isEmail().normalizeEmail().trim().escape()
        .custom( (value, {req}) =>{
  
          return User.findOne({email: value}).then(userData => {
            if(userData){
              return Promise.reject('This email already exist');
            }
          })
        }),
        
        check('password', "Password must be at least 5 characters and valid.").notEmpty().trim().escape().isLength({min:5,max:10}).custom( (value, {req}) => {
            
                if( value !== req.body.cpassword){
                    throw new Error('Confirm Password didn\'t match.')
                }
                return true;
        })
        
        
    ]
  }



