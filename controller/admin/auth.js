
const User = require('../../models/user');
const bcrypt = require('bcryptjs');
const { check, validationResult } = require('express-validator');




exports.getLogin = (req, res) =>{
    
    res.render('auth/login', {
        path: '/',
        pageTitle: 'Researcher Login',
        errMsg: flashMsg(req.flash('error'))
    });
}

exports.postLogin = (req, res, next) =>{
    let email = req.body.email;
    let password = req.body.password;
    let errors = validationResult(req);
    
    if( !errors.isEmpty() ){
        req.flash('error', errors.array()[0].msg)
        res.redirect('/super/login');
    }else{
        User.findOne({email: email}).then(user => {
            
            if( !user ){
              req.flash('error', 'Invailid Email and Password');
              return res.redirect('/super/login');
            }else{
                
                bcrypt.compare(password, user.password).then(doMatch => {

                    if(doMatch){
                      console.log(" ===== LoggedIN ===== ");
                    //   req.session.user = user;
                    //   req.session.isLoggedIn = true;
                        return res.redirect('/super/dashboard');
                    
                    }else{
                      req.flash('error', 'Invailid Email and Password');
                      return res.redirect('/super/login');
                    }
                })
            }
        }).catch(err => { 
            next(errorHandle(err)); 
        });
    }
    
}

exports.getLogout = (req, res, next) => {
    req.session.destroy(err => {
      console.log(err);
      res.redirect('/');
    });
};

let errorHandle = ( error)=>{
    let err = new Error(error);
    err.httpStatusCode = 500;
    return err;
}


let flashMsg = (val) =>{
  
    if(val.length >0){
      return val[0];
    }else{
      return null;
    }
  
}