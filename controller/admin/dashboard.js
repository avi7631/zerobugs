
exports.getDashboard = (req, res, next) =>{
    res.render('admin/dashboard',{
        path:'/dashboard',
        pageTitle: 'Dashboard'
    });
}