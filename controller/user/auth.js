
const User = require('../../models/user');
const bcrypt = require('bcryptjs');
const { check, validationResult } = require('express-validator');

exports.getSignup = (req, res, next) =>{

  res.render('./auth/signup',{
        path:'/signup',
        pageTitle:'Signup',
        errMsg: flashMsg(req.flash('error')) //flashMsg is function for show message
  });
  
}

exports.postSignup = (req, res, next) => {
    
    let name = req.body.name;
    let email = req.body.email;
    let password = req.body.password; 
    let errors = validationResult(req);

    if( !errors.isEmpty() ){
      
      return res.status(422).render('./auth/signup',{
        path:'/signup',
        pageTitle:'Signup',
        errMsg: errors.array()[0].msg 
      });

    }

    bcrypt.hash(password, 10).then(hashPass => {
        
        const userModel = new User({
          name:name,
          email: email,
          password: hashPass,
        });
        userModel.save();
      
    }).then( rs=> {
        console.log("User Registered Check the token In Console");
        res.redirect('/super/login');
      }).catch( err =>{
        next(errorHandle(err)); 
      });

  
}

exports.getLogin = (req, res) =>{
    
    res.render('auth/login', {
        path: '/',
        pageTitle: 'Researcher Login',
        errMsg: flashMsg(req.flash('error'))
    });
}

exports.postLogin = (req, res, next) =>{
    let email = req.body.email;
    let password = req.body.password;
    let errors = validationResult(req);
    
    if( !errors.isEmpty() ){
        req.flash('error', errors.array()[0].msg)
        res.redirect('/super/login');
    }else{
        User.findOne({email: email}).then(user => {
            
            if( !user ){
              req.flash('error', 'Invailid Email and Password');
              return res.redirect('/super/login');
            }else{
                
                bcrypt.compare(password, user.password).then(doMatch => {

                    if(doMatch){
                      
                      req.session.user = user;
                      req.session.isLoggedIn = true;
                      res.redirect('/');
                    
                    }else{
                      req.flash('error', 'Invailid Email and Password');
                      res.redirect('/super/dashboard');
                    }
                })
            }
        }).catch(err => { 
            next(errorHandle(err)); 
        });
    }
    
}


let errorHandle = ( error)=>{
    let err = new Error(error);
    err.httpStatusCode = 500;
    return err;
}


let flashMsg = (val) =>{
  
    if(val.length >0){
      return val[0];
    }else{
      return null;
    }
  
}